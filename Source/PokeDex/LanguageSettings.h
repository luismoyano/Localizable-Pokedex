// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "LanguageSettings.generated.h"

/**
*
*/
UCLASS()
class POKEDEX_API ULanguageSettings : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category = "Lang Settings")
		static bool setGameLanguage(FString lang);

	UFUNCTION(BlueprintPure, Category = "Lang Settings")
		static FString getGameLanguage();
private:



};
