// Fill out your copyright notice in the Description page of Project Settings.

#include "PokeDex.h"
#include "LanguageSettings.h"



bool ULanguageSettings::setGameLanguage(FString lang) {
	return FInternationalization::Get().SetCurrentCulture(lang);
}

FString ULanguageSettings::getGameLanguage() {
	return FInternationalization::Get().GetCurrentCulture()->GetName();
}




