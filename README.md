Localizable pokedex

This is an example of localization featuring hot switch developed in UE4.14.

It uses images and text extracted from http://pokemon.wikia.com/ so cheers to them.

This was developed only to provide guidance on how to perform text l10n and gender aware text in UE4

Feel free to use however you please as I own nothing from this piece.